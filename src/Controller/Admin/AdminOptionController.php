<?php

namespace App\Controller\Admin;

use App\Entity\Option;
use App\Form\OptionType;
use App\Repository\OptionRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/option")
 */
class AdminOptionController extends AbstractController
{
    /**
     * @var OptionRepository
     */
    private $repository;
    /**
     * @var ObjectManager
     */
    private $em;

    public function __construct(OptionRepository $repository, ObjectManager $em)
    {
        $this->repository = $repository;
        $this->em = $em;
    }

    /**
     * @Route("/", name="admin.option.index")
     */
    public function index()
    {
        $options = $this->repository->findAll();
        return $this->render('admin/option/index.html.twig', compact('options'));
    }


    /**
     * @Route("/new", name="admin.option.new")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function new(Request $request)
    {
        $option = new Option();
        $form = $this->createForm(OptionType::class, $option);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){
            $this->em->persist($option);
            $this->em->flush();
            $this->addFlash('success','Votre Option a bien ete ajouter');
            return $this->redirectToRoute('admin.option.index');
        }
        return $this->render('admin/option/new.html.twig', array(
            'option' => $option,
            'form'     => $form->createView()
        ));
    }

    /**
     * @Route("/{id}", name="admin.option.edit")
     * @param Option $option
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(Option $option, Request $request) :Response
    {
        $form = $this->createForm(OptionType::class, $option);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){
            $this->em->flush();
            $this->addFlash('success','Votre options a bien ete mis a jour');
            return $this->redirectToRoute('admin.option.index');
        }
        return $this->render('admin/option/edit.html.twig', array(
            'option' => $option,
            'form'     => $form->createView()
        ));
    }

    /**
     * @Route("/{id}", name="admin.option.delete")
     * @param Option $option
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function delete(Option $option, Request $request)
    {
        if($this->isCsrfTokenValid('delete' .$option->getId(), $request->get('_token'))){
            $this->em->remove($option);
            $this->em->flush();
            $this->addFlash('success','Votre option a bien ete supprimer');
        }
        return $this->redirectToRoute('admin.option.index');
    }
}
